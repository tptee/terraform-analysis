data "terraform_remote_state" "main" {
  backend = "s3"

  config = {
    bucket = "not-real-bucket"
    key    = "terraform/config/terraform/main/terraform.tfstate"
  }
}

locals {
  thing = data.terraform_remote_state.main.vpc_id
}

resource "nothingco_thing" "nada" {
  thing {
    attribute = "naaaaaah"

    stuff {
      nested = data.terraform_remote_state.main.kranch
    }
  }
}
