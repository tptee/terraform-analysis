package main

import (
	"log"
	"os"
	"path"
	"path/filepath"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/hashicorp/terraform-schema/module"
	tfschema "github.com/hashicorp/terraform-schema/schema"
)

func extractAttributeAccessPaths(file *hcl.File) (map[string][]string, hcl.Diagnostics) {
	// var paths []string
	pathsByDataSourceName := make(map[string][]string)

	diags := hclsyntax.VisitAll(file.Body.(hclsyntax.Node), func(node hclsyntax.Node) hcl.Diagnostics {
		if expr, ok := node.(*hclsyntax.Attribute); ok {
			variables := expr.Expr.Variables()

			for _, traversal := range variables {
				isDataSource := len(traversal) >= 3 && traversal.RootName() == "data"
				if !isDataSource {
					continue
				}

				dataSourceKind, ok := traversal[1].(hcl.TraverseAttr)
				isRemoteStateDataSource := ok && dataSourceKind.Name == "terraform_remote_state"
				if !isRemoteStateDataSource {
					continue
				}

				dataSourceName, ok := traversal[2].(hcl.TraverseAttr)
				if !ok {
					continue
				}

				path := ""

				for _, traverser := range traversal[3:] {
					if trav, ok := traverser.(hcl.TraverseAttr); ok {
						path += trav.Name
					}

					if trav, ok := traverser.(hcl.TraverseIndex); ok {
						path += "[" + trav.Key.AsString() + "]"
					}
				}

				pathsByDataSourceName[dataSourceName.Name] = append(pathsByDataSourceName[dataSourceName.Name], path)
			}
		}

		return nil
	})

	return pathsByDataSourceName, diags
}

type remoteStateDataSourceId struct {
	Name string
	Key  string
}

func extractRemoteStateIds(file *hcl.File, bodySchema *hcl.BodySchema) ([]remoteStateDataSourceId, hcl.Diagnostics) {
	var ids []remoteStateDataSourceId

	contents, diags := file.Body.Content(bodySchema)
	if diags.HasErrors() {
		return nil, diags
	}

	for _, block := range contents.Blocks.OfType("data") {
		isRemoteStateDataSource := len(block.Labels) == 2 && block.Labels[0] == "terraform_remote_state"
		if !isRemoteStateDataSource {
			continue
		}

		content, diags := block.Body.Content(&hcl.BodySchema{Attributes: []hcl.AttributeSchema{
			{Name: "backend", Required: true},
			{Name: "config", Required: true},
		}})
		if diags.HasErrors() {
			continue
		}

		config, diags := content.Attributes["config"].Expr.Value(nil)
		if diags.HasErrors() {
			continue
		}

		configMap := config.AsValueMap()
		if key, ok := configMap["key"]; ok {
			ids = append(ids, remoteStateDataSourceId{
				Name: block.Labels[1],
				Key:  key.AsString(),
			})
		}
	}

	return ids, diags
}

func main() {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	rootModulePath := path.Join(cwd, "test")

	rootModuleFiles, err := filepath.Glob(path.Join(rootModulePath, "*.tf"))
	if err != nil {
		log.Fatal(err)
	}

	coreSchema, err := tfschema.CoreModuleSchemaForVersion(tfschema.LatestAvailableVersion.Core())
	if err != nil {
		log.Fatal(err)
	}

	merger := tfschema.NewSchemaMerger(coreSchema)
	schema, err := merger.SchemaForModule(&module.Meta{Path: rootModulePath, Filenames: rootModuleFiles})
	if err != nil {
		log.Fatal(err)
	}

	for _, rootModuleFilePath := range rootModuleFiles {
		rootModuleFileContents, err := os.ReadFile(rootModuleFilePath)
		if err != nil {
			log.Fatal(err)
		}

		rootModuleFile, diags := hclsyntax.ParseConfig(
			rootModuleFileContents,
			rootModuleFilePath,
			hcl.InitialPos,
		)
		if diags.HasErrors() {
			log.Fatal(diags)
		}

		remoteStateAttributeAccessPaths, diags := extractAttributeAccessPaths(rootModuleFile)
		if diags.HasErrors() {
			log.Fatal(diags)
		}

		log.Println(remoteStateAttributeAccessPaths)

		remoteStateIds, diags := extractRemoteStateIds(rootModuleFile, schema.ToHCLSchema())
		if diags.HasErrors() {
			log.Fatal(diags)
		}

		log.Println(remoteStateIds)
	}
}
