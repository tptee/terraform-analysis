module terraform-analysis

go 1.22.2

require (
	github.com/hashicorp/hcl-lang v0.0.0-20240409100755-d90dc4d98974
	github.com/hashicorp/hcl/v2 v2.20.1
	github.com/hashicorp/terraform-config-inspect v0.0.0-20231204233900-a34142ec2a72
	github.com/hashicorp/terraform-json v0.21.0
	github.com/hashicorp/terraform-schema v0.0.0-20240419163338-44d6a2b3a686
	github.com/zclconf/go-cty v1.14.4
)

require (
	github.com/agext/levenshtein v1.2.2 // indirect
	github.com/apparentlymart/go-textseg/v15 v15.0.0 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/hashicorp/hcl v0.0.0-20170504190234-a4b07c25de5f // indirect
	github.com/hashicorp/terraform-registry-address v0.2.3 // indirect
	github.com/hashicorp/terraform-svchost v0.1.1 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/tools v0.20.0 // indirect
)
